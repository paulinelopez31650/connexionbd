Accès à la base de données :

    utilisateur : labege
    mot de passe : dwwm2020
    IP serveur : 10.45.2.149
    base de données : labege

Cette base de données ne contient qu'une table, nommée 'developer', dont les champs sont les suivants :

    id : clé primaire numérique auto-incrémentée
    firstname : champ texte de 50 caractères
    laststname : champ texte de 50 caractères
    email : champ texte de 100 caractères

    ressource : https://www.php.net/manual/fr/mysqli.examples-basic.php
